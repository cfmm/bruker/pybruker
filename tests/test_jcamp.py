"""
Copyright 2018-2024 Martyn Klassen

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import unittest
from pybruker.jcamp import JCAMPData
from pydicom import dcmread
from pydicom.tag import Tag
import os
import inspect


DATA_PATH = os.path.dirname(inspect.getabsfile(inspect.currentframe()))


class JCAMPTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        data = dcmread(os.path.join(DATA_PATH, "files", "FD08D5EC.dcm"))
        with open(os.path.join(DATA_PATH, "files", "method"), "wb") as fh:
            fh.write(data[Tag(0x0177, 0x1100)].value)
        with open(os.path.join(DATA_PATH, "files", "visu_pars"), "wb") as fh:
            fh.write(data[Tag(0x0177, 0x1101)].value)

    def test_read_file(self):
        data = JCAMPData(os.path.join(DATA_PATH, "files", "method"))
        self.check(data)
        data = JCAMPData(os.path.join(DATA_PATH, "files", "visu_pars"))
        self.check_visu_pars(data)

    def test_read_list(self):
        with open(os.path.join(DATA_PATH, "files", "method"), "r") as fh:
            data = fh.readlines()

        data = JCAMPData(data)
        self.check(data)

        with open(os.path.join(DATA_PATH, "files", "visu_pars"), "r") as fh:
            data = fh.readlines()

        data = JCAMPData(data)
        self.check_visu_pars(data)

    def test_read_string(self):
        with open(os.path.join(DATA_PATH, "files", "method"), "r") as fh:
            data = fh.read()

        data = JCAMPData(data)
        self.check(data)

        with open(os.path.join(DATA_PATH, "files", "visu_pars"), "r") as fh:
            data = fh.read()

        data = JCAMPData(data)
        self.check_visu_pars(data)

    def test_dicom_attribute(self):
        data = dcmread(os.path.join(DATA_PATH, "files", "FD08D5EC.dcm"))
        self.check(JCAMPData(data[Tag(0x0177, 0x1100)].value))
        self.check_visu_pars(JCAMPData(data[Tag(0x0177, 0x1101)].value))

    def check(self, data):
        self.assertNotIn("END", data, "Contains END variable")
        self.assertNotIn("end", data, "Contains end variable")
        self.assertIn("$PVM_MapShimCalcStat", data, "Missing $PVM_MapShimCalcStat")
        self.assertEqual(
            256,
            data["$PVM_MapShimCalcStat"]["maximum_length"],
            "$PVM_MapShimCalcStat length mismatch",
        )
        self.assertEqual(
            "<Not Calculated.>",
            data["$PVM_MapShimCalcStat"]["value"],
            "$PVM_MapShimCalcStat value does not match",
        )
        self.assertIsInstance(str(data), str)
        self.assertIsInstance(data.format("$PVM_EncSteps1"), object)
        self.assertIsInstance(data.format("$PVM_ScanTimeStr"), str)

    def check_visu_pars(self, data):
        pass

    def test_at_notation(self):
        data = "##$RECO_transposition=( 3 )\n@3*(0)"
        jdict = JCAMPData(data)
        self.assertEqual(jdict["$RECO_transposition"]["value"], [0, 0, 0])

        data = "##$RECO_transposition=( 5 )\n@2*(10) @3*(0.56)"
        jdict = JCAMPData(data)
        self.assertEqual(
            jdict["$RECO_transposition"]["value"], [10, 10, 0.56, 0.56, 0.56]
        )
