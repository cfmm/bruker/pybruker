"""
Copyright 2023-2024 Alan Kuurstra

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import unittest
from pybruker.BrukerViewer.imshow3d import main
import os
import inspect
import numpy as np
import sys

DATA_PATH = os.path.dirname(inspect.getabsfile(inspect.currentframe()))


class Imshow3dWithReloadTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.reco_dir = os.path.join(DATA_PATH, "files", "pdata", "1")
        if not os.path.exists(cls.reco_dir):
            os.makedirs(cls.reco_dir)
        reco_dir = cls.reco_dir
        r_sz = 2
        p_sz = 3
        s_sz = 4
        t_sz = 5
        matrix = [r_sz, p_sz, s_sz, t_sz]

        with open(os.path.join(reco_dir, "reco"), "wb") as fh:
            fh.write(b"##$RecoDim=2\n")
            fh.write(b"##$RECO_size=( 2 )\n")
            fh.write(f"{str(r_sz)} {str(p_sz)}\n".encode())
            fh.write(f"##$RecoObjectsPerRepetition={str(s_sz)}\n".encode())
            fh.write(f"##$RecoNumRepetitions={str(t_sz)}\n".encode())
            fh.write(b"##$RECO_byte_order=littleEndian\n")
            fh.write(b"##$RECO_wordtype=_16BIT_SGN_INT\n")
            fh.write(b"##$RECO_image_type=MAGNITUDE_IMAGE\n")
            fh.write(f"##$RECO_transposition=( {str(s_sz)} )\n".encode())
            for i in range(s_sz):
                fh.write(b"0 ")
            fh.write(b"\n")

        bruker_img = np.ndarray(matrix, dtype="<i2", order="C")
        bruker_img[:] = np.random.randn(*matrix) * 255

        # tofile always writes in 'C' order independent of the order of the original array
        # to write in 'F' order, we transpose a 'C' array
        bruker_img.T.tofile(os.path.join(reco_dir, "2dseq"))

    def test_main(self):
        if len(sys.argv) > 1:
            sys.argv[1] = self.reco_dir
        else:
            sys.argv.append(self.reco_dir)

        if len(sys.argv) > 2:
            sys.argv[2] = "0.5"
        else:
            sys.argv.append("0.5")

        if len(sys.argv) > 3:
            sys.argv[2] = "False"
        else:
            sys.argv.append("False")

        viewer = main()
        viewer.close()
