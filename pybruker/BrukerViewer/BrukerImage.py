"""
Copyright 2023-2024 Alan Kuurstra

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import os

import numpy as np

import pybruker.jcamp as jc


class BrukerImage:
    def __init__(self, imgdir):
        param_filename = imgdir + "/reco"
        self.param_dict = None
        visu_pars_filename = imgdir + "/visu_pars"
        self.visu_pars_dict = None
        self.data_filename = imgdir + "/2dseq"

        valid_file = False
        try:
            self.param_dict = jc.jcamp_read(param_filename)
            if self.param_dict:
                valid_file = True
        except BaseException:
            pass
        if not valid_file:
            raise ValueError("reco file is not valid")

        try:
            self.visu_pars_dict = jc.jcamp_read(visu_pars_filename)
            if self.visu_pars_dict:
                valid_file = True
        except BaseException:
            pass
        if not valid_file:
            raise ValueError("visu_pars file is not valid")

        if not os.path.exists(self.data_filename):
            raise ValueError("2dseq file does not exist")

    @property
    def dim_names(self):
        # Paravision manual: $RECO_transposition does not affect ordering of $RECO_size, just order when written to disc
        # $RECO_size is always [read,phase,slice]
        dim_order = ["PHASE", "READ"] if self.transpose else ["READ", "PHASE"]
        if self.param_dict["$RecoDim"] == 3:
            dim_order.append("SLICE")
        dim_order += [
            x[1][4:-1] for x in self.visu_pars_dict["$VisuFGOrderDesc"]["value"]
        ]
        return dim_order

    @property
    def dim_dict(self):
        dim_dict = {}
        # Paravision manual: $RECO_transposition does not affect ordering of $RECO_size, just order when written to disc
        # $RECO_size is always [read,phase,slice]
        dim_dict["READ"] = self.param_dict["$RECO_size"]["value"][0]
        dim_dict["PHASE"] = self.param_dict["$RECO_size"]["value"][1]
        if self.param_dict["$RecoDim"] == 3:
            dim_dict["SLICE"] = self.param_dict["$RECO_size"]["value"][2]
        dim_dict.update(
            {x[1][4:-1]: x[0] for x in self.visu_pars_dict["$VisuFGOrderDesc"]["value"]}
        )
        return dim_dict

    @property
    def dim_matrix(self):
        return [self.dim_dict[x] for x in self.dim_names]

    @property
    def aspect(self):
        # (read, phase, slice)
        aspect = np.array(
            self.visu_pars_dict["$VisuCoreExtent"]["value"][:2]
        ) / np.array(self.visu_pars_dict["$VisuCoreSize"]["value"][:2])
        return aspect

    @property
    def numpy_datatype(self):
        numpy_byte_order = ""
        if self.param_dict["$RECO_byte_order"] == "littleEndian":
            numpy_byte_order = "<"
        elif self.param_dict["$RECO_byte_order"] == "bigEndian":
            numpy_byte_order = ">"

        if self.param_dict["$RECO_wordtype"] == "_8BIT_UNSGN_INT":
            numpy_wordtype = "u1"
        elif self.param_dict["$RECO_wordtype"] == "_16BIT_SGN_INT":
            numpy_wordtype = "i2"
        elif self.param_dict["$RECO_wordtype"] == "_32BIT_SGN_INT":
            numpy_wordtype = "i4"
        elif self.param_dict["$RECO_wordtype"] == "_32BIT_FLOAT":
            numpy_wordtype = "f4"

        return np.dtype(numpy_byte_order + numpy_wordtype)

    @property
    def transpose(self):
        try:
            return self.param_dict["$RECO_transposition"]["value"][0]
        except BaseException:
            return 0

    @property
    def is_complex(self):
        return self.param_dict["$RECO_image_type"] == "COMPLEX_IMAGE"

    def get_num_cycles(self):
        return int(
            np.floor(
                os.path.getsize(self.data_filename)
                / (self.numpy_datatype.itemsize)
                / np.prod([y for x, y in self.dim_dict.items() if x != "CYCLE"])
            )
        )

    def load_cycles(self, number=None):
        matrix = self.dim_matrix
        if number:
            matrix[self.dim_names.index("CYCLE")] = number
        with open(self.data_filename, mode="rb") as f:
            img = np.fromfile(f, self.numpy_datatype, count=np.prod(matrix))
        img = img.reshape(matrix, order="F")
        if self.is_complex:
            complex_dim = self.dim_names.index("COMPLEX")
            # take makes copies, indexing creates views
            # img = np.take(img, 0, axis=complex_dim) + 1j * np.take(img, 1, axis=complex_dim)
            r_indx = [np.s_[:]] * img.ndim
            r_indx[complex_dim] = np.s_[0]
            i_indx = [np.s_[:]] * img.ndim
            i_indx[complex_dim] = np.s_[1]
            img = img[tuple(r_indx)] + 1j * img[tuple(i_indx)]
        return img

    @property
    def data(self):
        return self.load_cycles()
