#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Copyright 2018-2024 Alan Kuurstra

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on Tue Nov  6 08:58:47 2018

@author: akuurstr
"""
import os
import sys

import numpy as np
from PyQt5 import QtWidgets
from vidi3d.core import start_viewer
from vidi3d.imshow.main import Imshow3d as Imshow3dBase

from pybruker.BrukerViewer.UnfinishedEpi import UnfinishedEpi


class Imshow3dWithReload(Imshow3dBase):
    def __init__(
        self, imgdir, background_threshold=0.5, pixdim=None, interpolation="bicubic"
    ):
        self.epi = UnfinishedEpi(imgdir)

        if pixdim is None:
            pixdim = self.epi.aspect

        complex_img = self.load_available_volumes()

        Imshow3dBase.__init__(
            self,
            complex_img,
            background_threshold=background_threshold,
            pixdim=pixdim,
            interpolation=interpolation,
        )

        # add reload button
        button = QtWidgets.QPushButton("Load more volumes")
        button.clicked.connect(self.reload_data)
        self.controls.wl_layout.parent().addWidget(button)
        self.controls.wl_layout.parent().addStretch()

        # change dimension text
        xtext = "r"
        ytext = "p"
        ztext = "s"
        ttext = "t"
        self.image4d.zslice.title.set_text(ztext + " cross-section")
        self.image4d.zslice.htxt.set_text(xtext)
        self.image4d.zslice.vtxt.set_text(ytext)

        self.image4d.xslice.title.set_text(xtext + " cross-section")
        self.image4d.xslice.htxt.set_text(ztext)
        self.image4d.xslice.vtxt.set_text(ytext)

        self.image4d.yslice.title.set_text(ytext + " cross-section")
        self.image4d.yslice.htxt.set_text(xtext)
        self.image4d.yslice.vtxt.set_text(ztext)

        self.image4d.xplot.axes.set_title(xtext + " plot")
        self.image4d.yplot.axes.set_title(ytext + " plot")
        self.image4d.zplot.axes.set_title(ztext + " plot")
        self.image4d.tplot.axes.set_title(ttext + " plot")

        # hacky, but I don't want to refactor vidi3d
        tmp = self.controls.children()[0].children()[1]
        tmp.itemAt(0).widget().setText(xtext)
        tmp.itemAt(2).widget().setText(ytext)
        tmp.itemAt(4).widget().setText(ztext)
        tmp.itemAt(6).widget().setText(ttext)

    def load_available_volumes(self):
        complex_img = self.epi.load_available_volumes()
        if self.epi.transpose:
            np.swapaxes(complex_img, 0, 1)
        return complex_img

    def reload_data(self):
        num_available_volumes = self.epi.get_num_cycles()
        if num_available_volumes != self.image4d.complex_image.shape[-1]:
            self.image4d.complex_image = self.load_available_volumes()

            self.controls.tcontrol.setMaximum(num_available_volumes - 1)
            # it's not enough to rewrite the plot's "lines" list using createLines()
            # must also use remove() on all elements in previous list to delete them
            # from being redrawn
            self.image4d.tplot.lines[0][0].remove()
            new_Tplot_line_list = [
                self.image4d.complex_image[
                    self.image4d.cursor_loc.x,
                    self.image4d.cursor_loc.y,
                    self.image4d.cursor_loc.z,
                    :,
                ],
            ]
            self.image4d.tplot.set_complex_data(new_Tplot_line_list)
            self.image4d.tplot.create_lines()
            self.image4d.tplot.draw_lines_and_markers()


def imshow3d(
    imgdir, background_threshold=0.5, pixdim=None, interpolation="none", block=True
):
    """
    A viewer that displays cross sections of a 3D image.

    Parameters
    -----------
    data : array_like, shape (x, y, z)
        The data to be displayed.

    pixdim : list of voxel sizes for each dimesion. For nifti files this
             can be found using: nib.load(imgloc).get_header()['pixdim'][1:4]

    interpolation : string, optional, default: 'none'
        Acceptable values are 'none', 'nearest', 'bilinear', 'bicubic',
        'spline16', 'spline36', 'hanning', 'hamming', 'hermite', 'kaiser',
        'quadric', 'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc',
        'lanczos'

        If `interpolation` is 'none', then no interpolation is performed
        on the Agg, ps and pdf backends. Other backends will fall back to
        'nearest'.

    block : boolean, optional, default: False
        If true, block execution of further code until all viewers are closed.

    Returns
    --------
    viewer : `Imshow3dWithReload`

    """

    viewer = Imshow3dWithReload(
        imgdir,
        background_threshold=background_threshold,
        pixdim=pixdim,
        interpolation=interpolation,
    )

    if not block:
        viewer.image4d.complex_image = np.copy(viewer.image4d.complex_image)
        # if the viewer is run as not blocking, then the underlying data
        # can change later on in the script and effect the results shown
        # in the viewer.  Therefore, we must make a copy.  If you have a
        # large data set and don't want to wait for the copy or can't afford
        # the memory, then you should run the viewer with block=True
    return start_viewer(viewer, block)


def main():
    debug = False
    imgdir = sys.argv[1]
    bg_thresh = sys.argv[2] if len(sys.argv) > 2 else None
    block = sys.argv[3] if len(sys.argv) > 3 else "True"
    if imgdir.lower() == "debug":
        debug = True
        imgdir = sys.argv[2]
        bg_thresh = True if len(sys.argv) > 3 else None
        block = sys.argv[4] if len(sys.argv) > 4 else "True"
    bg_thresh = 0.5 if bg_thresh is None else bg_thresh
    if not os.path.exists(imgdir):
        raise FileNotFoundError("Please provide a valid image directory")
    try:
        bg_thresh = float(bg_thresh)
    except ValueError:
        raise ValueError("Please provide a valid background threshold")
    if not block.lower() in ["true", "1", "t", "false", "0", "f"]:
        raise ValueError("Please provide a valid 'block' boolean")
    block = True if block.lower() in ["true", "1", "t"] else False
    if debug:
        return imshow3d(imgdir, background_threshold=bg_thresh, block=block)
    else:
        with open(os.devnull, "w") as devnull:
            stdout_orig = sys.stdout
            stderr_orig = sys.stderr
            sys.stdout = devnull
            sys.stderr = devnull
            retval = imshow3d(imgdir, background_threshold=bg_thresh, block=block)
            sys.stdout = stdout_orig
            sys.stderr = stderr_orig
            return retval


if __name__ == "__main__":
    main()
