"""
Copyright 2023-2024 Alan Kuurstra

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import numpy as np

from pybruker.BrukerViewer.BrukerImage import BrukerImage
import pybruker.jcamp as jc
import os


class UnfinishedEpi(BrukerImage):
    def __init__(self, imgdir):
        # the following is the same as super().__init__() but with visu_pars information removed
        param_filename = imgdir + "/reco"
        self.param_dict = None
        self.data_filename = imgdir + "/2dseq"

        valid_file = False
        try:
            self.param_dict = jc.jcamp_read(param_filename)
            if self.param_dict:
                valid_file = True
        except BaseException:
            pass
        if not valid_file:
            raise ValueError("reco file is not valid")

        if not os.path.exists(self.data_filename):
            raise ValueError("2dseq file does not exist")

        method_filename = imgdir + "/../../method"
        try:
            self.method_dict = jc.jcamp_read(method_filename)
        except BaseException:
            print("method file is not valid, cannot retrieve proper voxel size")
            self.method_dict = None

    @property
    def aspect(self):
        if self.method_dict:
            return self.method_dict["$PVM_SpatResol"]["value"] + [
                self.method_dict["$PVM_SliceThick"],
            ]
        return None

    @property
    def dim_names(self):
        # Paravision manual: $RECO_transposition does not affect ordering of $RECO_size, just order when written to disc
        # $RECO_size is always [read,phase,slice]
        dim_order = ["PHASE", "READ"] if self.transpose else ["READ", "PHASE"]

        dim_order.extend(["SLICE", "CYCLE"])
        if self.is_complex:
            dim_order.append("COMPLEX")
        return dim_order

    @property
    def dim_dict(self):
        dim_dict = {}
        # Paravision manual: $RECO_transposition does not affect ordering of $RECO_size, just order when written to disc
        # $RECO_size is always [read,phase,slice]
        dim_dict["READ"] = self.param_dict["$RECO_size"]["value"][0]
        dim_dict["PHASE"] = self.param_dict["$RECO_size"]["value"][1]
        if self.param_dict["$RecoDim"] == 3:
            dim_dict["SLICE"] = self.param_dict["$RECO_size"]["value"][2]

        else:
            dim_dict["SLICE"] = self.param_dict["$RecoObjectsPerRepetition"]
        dim_dict["CYCLE"] = self.param_dict["$RecoNumRepetitions"]
        if self.is_complex:
            dim_dict["COMPLEX"] = 2
        return dim_dict

    def load_available_volumes(self):
        n = self.get_num_cycles()
        if n == 0:
            return 0
        else:
            vols = self.load_cycles(n)
            # UnfinishedEpi always returns the volumes in read, phase, slice order
            if self.transpose:
                vols = np.swapaxes(vols, 0, 1)
            return vols
