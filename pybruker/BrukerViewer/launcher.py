"""
Copyright 2023-2024 Alan Kuurstra

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets
from vidi3d import compare2d
import sys
import os

from pybruker.BrukerViewer.BrukerImage import BrukerImage
from pybruker.BrukerViewer.QDragDropListWidget import QDragDropListWidget


class SliceValidator(QtGui.QIntValidator):
    def validate(self, p_str, p_int):
        # allow an empty string to be valid so that user can backspace to an empty box
        # catch empty strings in the slice_changed() callback
        if not p_str:
            return (QtGui.QValidator.Acceptable, p_str, p_int)
        if p_str.isdigit():
            val = int(p_str)
            if val >= self.bottom() and val <= self.top():
                return (QtGui.QValidator.Acceptable, str(val), p_int)
        return (QtGui.QValidator.Invalid, p_str, p_int)


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        self.main_window = MainWindow
        MainWindow.setObjectName("MainWindow")
        MainWindow.setWindowTitle(_translate("MainWindow", "ViewerLauncher"))
        MainWindow.resize(543, 552)
        font = QtGui.QFont()
        font.setFamily("Sans Serif")

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.horizontalLayoutWidget_3 = QtWidgets.QWidget(self.centralwidget)
        self.horizontalLayoutWidget_3.setGeometry(QtCore.QRect(10, 10, 471, 31))
        self.horizontalLayoutWidget_3.setObjectName("horizontalLayoutWidget_3")
        self.directory_layout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget_3)
        self.directory_layout.setContentsMargins(0, 0, 0, 0)
        self.directory_layout.setObjectName("directory_layout")
        self.directory_label = QtWidgets.QLabel(self.horizontalLayoutWidget_3)
        self.directory_label.setObjectName("directory_label")
        self.directory_label.setText(_translate("MainWindow", "Scan Directory"))
        self.directory_layout.addWidget(self.directory_label)
        self.directory_text = QtWidgets.QTextEdit(self.horizontalLayoutWidget_3)
        self.directory_text.setObjectName("directory_text")
        self.directory_layout.addWidget(self.directory_text)
        self.directory_button = QtWidgets.QToolButton(self.horizontalLayoutWidget_3)
        self.directory_button.setObjectName("directory_button")
        self.directory_button.setText(_translate("MainWindow", "..."))
        self.directory_button.clicked.connect(self.directory_button_pushed)
        self.directory_layout.addWidget(self.directory_button)

        self.verticalLayoutWidget_2 = QtWidgets.QWidget(self.centralwidget)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(10, 80, 521, 461))
        self.verticalLayoutWidget_2.setObjectName("verticalLayoutWidget_2")

        self.main_layout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget_2)
        self.main_layout.setContentsMargins(0, 0, 0, 0)
        self.main_layout.setObjectName("main_layout")

        self.dimension_layout = QtWidgets.QGridLayout()
        self.dimension_layout.setObjectName("dimension_layout")

        label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        label.setText("Sliced dimensions")
        self.dimension_layout.addWidget(label, 0, 0)

        label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        label.setText("Slice")
        self.dimension_layout.addWidget(label, 0, 1)

        label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        label.setText("Viewer dimensions")
        self.dimension_layout.addWidget(label, 0, 3)

        self.dimensions_list = QDragDropListWidget(self.verticalLayoutWidget_2)
        self.dimensions_list.setObjectName("dimensions_list")
        self.dimensions_list.model().rowsMoved.connect(self.refresh_slice_line_edit)
        self.dimensions_list.model().rowsRemoved.connect(self.refresh_slice_line_edit)
        # rowsInserted signal is emitted before the row text is updated
        # inserted row value does not have a dimension name required by refresh_slice_line_edit
        # self.dimensions_list.model().rowsInserted.connect(self.refresh_slice_line_edit)
        # hacky workaround is to connect x/y/z/t/tile rowsRemoved signals to refresh_slice_line_edit
        # when we move dimensions between eg. x and y lists, this will unnecessarily call refresh_slice_line_edit

        self.dimension_layout.addWidget(self.dimensions_list, 1, 0)

        self.slice_select_layout = QtWidgets.QVBoxLayout()
        slice_select_layout = self.slice_select_layout
        slice_select_layout.addItem(
            QtWidgets.QSpacerItem(
                1, 5, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum
            )
        )
        for n in range(13):
            tmp = QtWidgets.QLineEdit()
            fm = tmp.fontMetrics()
            m = tmp.textMargins()
            c = tmp.contentsMargins()
            w = 4 * fm.width("x") + m.left() + m.right() + c.left() + c.right()
            tmp.setMaximumWidth(w + 8)
            tmp.setEnabled(False)
            tmp.editingFinished.connect(self.slice_changed)
            slice_select_layout.addWidget(tmp)

        slice_select_layout.addItem(
            QtWidgets.QSpacerItem(
                1, 12, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum
            )
        )
        self.dimension_layout.addLayout(slice_select_layout, 1, 1)

        spacerItem = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding
        )
        self.dimension_layout.addItem(spacerItem, 1, 2)

        self.viewer_dim_layout = QtWidgets.QGridLayout()
        self.viewer_dim_layout.setObjectName("viewer_dim_layout")

        self.x_label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.x_label.setObjectName("x_label")
        self.x_label.setText(_translate("MainWindow", "X"))
        self.x_label.setAlignment(QtCore.Qt.AlignTop)
        self.x_label.setAlignment(QtCore.Qt.AlignRight)
        self.viewer_dim_layout.addWidget(self.x_label, 0, 1, 1, 1)

        self.y_label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.y_label.setObjectName("y_label")
        self.y_label.setText(_translate("MainWindow", "Y"))
        self.y_label.setAlignment(QtCore.Qt.AlignTop)
        self.y_label.setAlignment(QtCore.Qt.AlignRight)
        self.viewer_dim_layout.addWidget(self.y_label, 1, 1, 1, 1)

        self.z_label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.z_label.setObjectName("z_label")
        self.z_label.setText(_translate("MainWindow", "Z"))
        self.z_label.setAlignment(QtCore.Qt.AlignTop)
        self.z_label.setAlignment(QtCore.Qt.AlignRight)
        self.viewer_dim_layout.addWidget(self.z_label, 2, 1, 1, 1)

        self.t_label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.t_label.setObjectName("t_label")
        self.t_label.setText(_translate("MainWindow", "T"))
        self.t_label.setAlignment(QtCore.Qt.AlignTop)
        self.t_label.setAlignment(QtCore.Qt.AlignRight)
        self.viewer_dim_layout.addWidget(self.t_label, 3, 1, 1, 1)

        self.tile_label = QtWidgets.QLabel(self.verticalLayoutWidget_2)
        self.tile_label.setObjectName("tile_label")
        self.tile_label.setText(_translate("MainWindow", "tile"))
        self.tile_label.setAlignment(QtCore.Qt.AlignTop)
        self.tile_label.setAlignment(QtCore.Qt.AlignRight)
        self.viewer_dim_layout.addWidget(self.tile_label, 4, 1, 1, 1)

        self.x_list = QDragDropListWidget(self.verticalLayoutWidget_2)
        self.x_list.setFixedHeight(35)
        self.x_list.setObjectName("x_list")
        self.x_list.model().rowsRemoved.connect(self.refresh_slice_line_edit)
        self.viewer_dim_layout.addWidget(self.x_list, 0, 3, 1, 1)

        self.y_list = QDragDropListWidget(self.verticalLayoutWidget_2)
        self.y_list.setFixedHeight(35)
        self.y_list.setObjectName("y_list")
        self.y_list.model().rowsRemoved.connect(self.refresh_slice_line_edit)
        self.viewer_dim_layout.addWidget(self.y_list, 1, 3, 1, 1)

        self.z_list = QDragDropListWidget(self.verticalLayoutWidget_2)
        self.z_list.setFixedHeight(35)
        self.z_list.setObjectName("z_list")
        self.z_list.model().rowsRemoved.connect(self.refresh_slice_line_edit)
        self.viewer_dim_layout.addWidget(self.z_list, 2, 3, 1, 1)

        self.t_list = QDragDropListWidget(self.verticalLayoutWidget_2)
        self.t_list.setAcceptDrops(True)
        self.t_list.setObjectName("t_list")
        self.t_list.model().rowsRemoved.connect(self.refresh_slice_line_edit)
        self.viewer_dim_layout.addWidget(self.t_list, 3, 3, 1, 1)

        self.tile_list = QDragDropListWidget(self.verticalLayoutWidget_2)
        self.tile_list.setObjectName("tile_list")
        self.tile_list.model().rowsRemoved.connect(self.refresh_slice_line_edit)
        self.viewer_dim_layout.addWidget(self.tile_list, 4, 3, 1, 1)

        tile_opt_layout = QtWidgets.QHBoxLayout()
        tmp = QtWidgets.QLabel()
        tmp.setText("start")
        tile_opt_layout.addWidget(tmp)
        self.tile_start = QtWidgets.QSpinBox()
        self.tile_start.valueChanged.connect(self.tile_slicing_changed)
        tile_opt_layout.addWidget(self.tile_start)
        tmp = QtWidgets.QLabel()
        tmp.setText("stop")
        tile_opt_layout.addWidget(tmp)
        self.tile_stop = QtWidgets.QSpinBox()
        self.tile_stop.valueChanged.connect(self.tile_slicing_changed)
        tile_opt_layout.addWidget(self.tile_stop)
        tmp = QtWidgets.QLabel()
        tmp.setText("int.")
        tile_opt_layout.addWidget(tmp)
        self.tile_interval = QtWidgets.QSpinBox()
        tile_opt_layout.addWidget(self.tile_interval)
        self.viewer_dim_layout.addLayout(tile_opt_layout, 5, 3, 1, 1)

        self.dimension_layout.addLayout(self.viewer_dim_layout, 1, 3)
        self.main_layout.addLayout(self.dimension_layout)
        spacerItem1 = QtWidgets.QSpacerItem(
            40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum
        )
        self.main_layout.addItem(spacerItem1)

        self.bottom_layout = QtWidgets.QHBoxLayout()
        self.bottom_layout.setObjectName("bottom_layout")

        self.scan_info = QtWidgets.QGridLayout()
        self.scan_info.setObjectName("scan_info")
        self.scan_info_names = [
            "VisuSubjectId",
            "VisuSeriesDate",
            "VisuSubjectPosition",
            "VisuAcqSequenceName",
            "VisuAcquisitionProtocol",
            "RecoDim",
            "RECO_image_type",
            # 'VisuCoilTransmitName',
            # 'VisuCoilReceiveName',
        ]

        tmp = QtWidgets.QLabel()
        tmp.setText("Scan Info")
        font.setPointSize(5)
        font.setBold(True)
        tmp.setFont(font)
        font.setBold(False)
        self.scan_info.addWidget(tmp, 0, 0)

        for indx in range(len(self.scan_info_names)):
            tmp = QtWidgets.QLabel()
            tmp.setText(f"{self.scan_info_names[indx]}: ")
            tmp.setFont(font)
            self.scan_info.addWidget(tmp, indx + 1, 0)

            tmp = QtWidgets.QLabel()
            tmp.setFont(font)
            self.scan_info.addWidget(tmp, indx + 1, 1)

        self.bottom_layout.addLayout(self.scan_info)
        spacerItem2 = QtWidgets.QSpacerItem(
            20, 40, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding
        )
        self.bottom_layout.addItem(spacerItem2)
        self.bottom_right_layout = QtWidgets.QVBoxLayout()
        self.bottom_right_layout.setObjectName("bottom_right_layout")

        self.fourier_checkbox = QtWidgets.QCheckBox(self.verticalLayoutWidget_2)
        font.setPointSize(10)
        font.setBold(True)
        self.fourier_checkbox.setFont(font)
        font.setBold(False)
        self.fourier_checkbox.setObjectName("fourier_checkbox")
        self.fourier_checkbox.setText(
            _translate("MainWindow", "View  in Fourier Domain")
        )
        self.bottom_right_layout.addWidget(self.fourier_checkbox)

        self.launch_button = QtWidgets.QPushButton(self.verticalLayoutWidget_2)
        self.launch_button.clicked.connect(self.launch_button_pushed)
        font.setPointSize(14)
        self.launch_button.setFont(font)
        self.launch_button.setStyleSheet("background-color : green")
        self.launch_button.setObjectName("launch_button")
        self.launch_button.setText(_translate("MainWindow", "Launch Viewer"))
        self.bottom_right_layout.addWidget(self.launch_button)

        self.bottom_layout.addLayout(self.bottom_right_layout)
        self.main_layout.addLayout(self.bottom_layout)

        MainWindow.setCentralWidget(self.centralwidget)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        self.bruker_image = None
        self.refresh_slice_line_edit()

    def directory_button_pushed(self):
        dir = QtWidgets.QFileDialog.getExistingDirectory(
            ui.main_window,
            "Select Directory",
            self.directory_text.toPlainText(),
            options=QtWidgets.QFileDialog.DontUseNativeDialog,
        )
        self.set_directory(dir)

    def set_directory(self, dir):
        font = QtGui.QFont()
        try:
            self.bruker_image = BrukerImage(dir)
        except ValueError as err:
            print(err)
            return
        self.directory_text.setText(dir)

        # clear existing lists
        for curr_list in [
            self.dimensions_list,
            self.x_list,
            self.y_list,
            self.z_list,
            self.t_list,
            self.tile_list,
        ]:
            curr_list.clear()
        # clear fourier checkbox
        self.fourier_checkbox.setChecked(False)

        dim_names = [x for x in self.bruker_image.dim_names if x != "COMPLEX"]
        self.dim_range = {name: self.bruker_image.dim_dict[name] for name in dim_names}
        self.dim_slice = {name: 0 for name in dim_names}
        font.setPointSize(14)
        self.dimensions_list.setFont(font)
        for name in dim_names:
            self.dimensions_list.addItem(name)

        if self.bruker_image.is_complex:
            self.fourier_checkbox.setEnabled(True)
        else:
            self.fourier_checkbox.setEnabled(False)
        self.refresh_slice_line_edit()

        font.setPointSize(5)
        for indx in range(len(self.scan_info_names)):
            info_name = self.scan_info_names[indx]
            if info_name[0] == "R":
                info_value = self.bruker_image.param_dict[f"${info_name}"]
            elif info_name[0] == "V":
                info_value = self.bruker_image.visu_pars_dict[f"${info_name}"]
            else:
                continue
            if type(info_value) == dict:
                info_value = info_value["value"]
            self.scan_info.itemAtPosition(indx + 1, 1).widget().setText(str(info_value))

    def refresh_slice_line_edit(self):
        if self.tile_list.count():
            self.tile_start.setEnabled(True)
            self.tile_stop.setEnabled(True)
            self.tile_interval.setEnabled(True)
            self.tile_start.setMinimum(0)
            concat_sz = 1
            for indx in range(self.tile_list.count()):
                dim_name = self.tile_list.item(indx).text()
                concat_sz *= self.dim_range[dim_name]
            self.tile_stop.setMaximum(concat_sz - 1)
            self.tile_stop.setValue(concat_sz)
            self.tile_interval.setMinimum(1)
            self.tile_slicing_changed()
        else:
            self.tile_start.setValue(0)
            self.tile_stop.setValue(0)
            self.tile_interval.setValue(0)
            self.tile_start.setEnabled(False)
            self.tile_stop.setEnabled(False)
            self.tile_interval.setEnabled(False)
        for indx in range(self.slice_select_layout.count() - 2):
            line_edit = self.slice_select_layout.itemAt(indx + 1).widget()
            line_edit.setText("")
            line_edit.setEnabled(False)
        for indx in range(self.dimensions_list.count()):
            line_edit = self.slice_select_layout.itemAt(indx + 1).widget()
            dim_name = self.dimensions_list.item(indx).text()
            line_edit.setValidator(SliceValidator(0, self.dim_range[dim_name] - 1))
            line_edit.setText(str(self.dim_slice[dim_name]))
            line_edit.setEnabled(True)
            line_edit.update()

    def slice_changed(self):
        for indx in range(self.dimensions_list.count()):
            line_edit = self.slice_select_layout.itemAt(indx + 1).widget()
            dim_name = self.dimensions_list.item(indx).text()
            if line_edit.text():
                self.dim_slice[dim_name] = int(line_edit.text())
            line_edit.setText(str(self.dim_slice[dim_name]))

    def tile_slicing_changed(self):
        start_val = self.tile_start.value()
        stop_val = self.tile_stop.value()
        self.tile_stop.setMinimum(start_val + 1)
        self.tile_start.setMaximum(stop_val - 1)
        self.tile_interval.setMaximum(stop_val - start_val)

    def launch_button_pushed(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Warning)
        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
        if not self.bruker_image:
            msg.setText("Please select a valid scan directory.")
            msg.exec_()
            return

        if not self.x_list.count():
            msg.setText("X dimension required.")
            msg.exec_()
            return

        if not self.y_list.count():
            msg.setText("Y dimension required.")
            msg.exec_()
            return

        volume = self.bruker_image.data
        dim_names = self.bruker_image.dim_names
        if "COMPLEX" in dim_names:
            dim_names.remove("COMPLEX")

        if self.bruker_image.param_dict["$RecoDim"] == 2:
            fourier_dims = ["READ", "PHASE"]
            fourier_axes = [0, 1]
        elif self.bruker_image.param_dict["$RecoDim"] == 3:
            fourier_dims = ["READ", "PHASE", "SLICE"]
            fourier_axes = [0, 1, 2]

        # seperate sliced dimensions that are potentially involved in fft
        nonfourier_slice_dims = [
            self.dimensions_list.item(indx).text()
            for indx in range(self.dimensions_list.count())
        ]
        fourier_slice_dims = []
        for fourier_dim in fourier_dims:
            try:
                fourier_slice_dims.append(
                    nonfourier_slice_dims.pop(nonfourier_slice_dims.index(fourier_dim))
                )
            except:
                pass
        # slice dimensions not involved in fft
        slices = [np.s_[:]] * volume.ndim
        for dim_name in nonfourier_slice_dims:
            slice_indx = dim_names.index(dim_name)
            slice_val = self.dim_slice[dim_name]
            # take makes a copy, slices create a view
            # volume = np.take(volume,slice_val,axis=slice_indx)
            slices[slice_indx] = np.s_[slice_val]
        volume = volume[tuple(slices)]
        [dim_names.remove(x) for x in nonfourier_slice_dims]
        # optional fft operation
        if self.bruker_image.is_complex and self.fourier_checkbox.isChecked():
            volume = np.fft.fftn(volume, axes=fourier_axes)
            volume = np.fft.fftshift(volume, axes=fourier_axes)
        # slice dimensions potentially involved in fft
        slices = [np.s_[:]] * volume.ndim
        for dim_name in fourier_slice_dims:
            slice_indx = dim_names.index(dim_name)
            slice_val = self.dim_slice[dim_name]
            # take makes a copy, slices create a view
            # volume = np.take(volume,slice_val,axis=slice_indx)
            slices[slice_indx] = np.s_[slice_val]
        volume = volume[tuple(slices)]
        [dim_names.remove(x) for x in fourier_slice_dims]

        # transpose viewer dimensions and concatenate
        concat_dim_names = []
        for curr_list in [
            self.x_list,
            self.y_list,
            self.z_list,
            self.t_list,
            self.tile_list,
        ]:
            concat_dim_names.append(
                [curr_list.item(indx).text() for indx in range(curr_list.count())]
            )
        transpose_order = [
            dim_names.index(name)
            for dim_group in concat_dim_names
            for name in dim_group
        ]
        volume = volume.transpose(transpose_order)
        # concatenate dimensions
        reshape_list = []
        for dim_group in concat_dim_names:
            reshape_list.append(
                int(
                    np.prod(
                        [self.bruker_image.dim_dict[dim_name] for dim_name in dim_group]
                    )
                )
            )
        volume = volume.reshape(reshape_list, order="F")
        # slice tile dimension
        if self.tile_list.count():
            volume = volume[
                np.s_[
                    ...,
                    self.tile_start.value() : self.tile_stop.value()
                    + 1 : self.tile_interval.value(),
                ]
            ]
        compare2d(
            [img[..., 0] for img in np.split(volume, volume.shape[4], axis=4)],
            block=False,
            subplot_titles=range(
                self.tile_start.value(),
                self.tile_stop.value() + 1,
                self.tile_interval.value(),
            ),
        )


if __name__ == "__main__":
    debug = False
    imgdir = sys.argv[1] if len(sys.argv) > 1 else ""
    bg_thresh = sys.argv[2] if len(sys.argv) > 2 else None
    block = sys.argv[3] if len(sys.argv) > 3 else "True"
    if imgdir.lower() == "debug":
        debug = True
        imgdir = sys.argv[2]
        bg_thresh = True if len(sys.argv) > 3 else None
        block = sys.argv[4] if len(sys.argv) > 4 else "True"
    bg_thresh = 0.5 if bg_thresh is None else bg_thresh
    if not os.path.exists(imgdir):
        imgdir = ""
    try:
        bg_thresh = float(bg_thresh)
    except ValueError:
        raise ValueError("Please provide a valid background threshold")
    if not block.lower() in ["true", "1", "t", "false", "0", "f"]:
        raise ValueError("Please provide a valid 'block' boolean")
    block = True if block.lower() in ["true", "1", "t"] else False
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()

    def run_gui():
        ui.setupUi(MainWindow)
        ui.set_directory(imgdir)
        MainWindow.show()
        sys.exit(app.exec_())

    if debug:
        run_gui()
    else:
        with open(os.devnull, "w") as devnull:
            stdout_orig = sys.stdout
            stderr_orig = sys.stderr
            sys.stdout = devnull
            sys.stderr = devnull
            run_gui()
