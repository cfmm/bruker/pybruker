"""
Copyright 2023-2024 Alan Kuurstra

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from PyQt5 import QtCore, QtWidgets


class QDragDropListWidget(QtWidgets.QListWidget):
    dropEventSignal = QtCore.pyqtSignal()

    def __init__(self, type, parent=None):
        super(QDragDropListWidget, self).__init__(parent)
        self.setIconSize(QtCore.QSize(124, 124))
        self.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.setAcceptDrops(True)

    def dragMoveEvent(self, event):
        # shows a line where the insert will happen
        super(QDragDropListWidget, self).dragMoveEvent(event)

    def dropEvent(self, event):
        # # it seems no drop actions are supported!
        # print(self.supportedDropActions())
        # print(event.possibleActions())
        #
        # # this sets event.DropAction() to move, which apparently is not a supported action
        # event.setDropAction(QtCore.Qt.MoveAction)

        # since no action is supported/possible, the parent will only perform the default operation
        super(QDragDropListWidget, self).dropEvent(event)

        # this signal is emitted before the source list removes the item
        self.dropEventSignal.emit()
