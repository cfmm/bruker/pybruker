#!/usr/bin/env python
"""
Copyright 2024 Martyn Klassen

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
from setuptools import setup
import os


def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="pybruker",
    version=os.getenv("CI_COMMIT_TAG") or "0.0.1",
    description="Python tools for manipulating Bruker Paravision Data",
    author="Martyn Klassen",
    author_email="lmklassen@gmail.com",
    # url='https://www.python.org/sigs/distutils-sig/',
    license="MIT",
    keywords="bruker paravision",
    packages=[
        "pybruker",
        "pybruker.BrukerViewer",
    ],
    install_requires=[
        "pydicom~=2.2.1",
        "vidi3d~=1.3.3",
    ],
    extras_require={"dev": ["black", "pre-commit"]},
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 4 - Beta",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
    ],
)
